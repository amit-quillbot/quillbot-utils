const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: './index.js',
  mode: 'development',
  output: {
    libraryTarget: 'system',
    path: path.resolve('dist'),
    filename: 'isomorphic-mf-quillbot-utils.js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
  externals: [
    "react",
  ],
  devServer: {
    port: 4006,
  },
};
